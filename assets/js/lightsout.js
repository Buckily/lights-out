$(document).ready(function() {
    var lightsout = {
        'board': $('#lightsout'),
        'size': 5,
        'game': [],
        'clicks': 0,
        'toggle': function(tile) {
            lightsout.game[tile] ^= 1;
            if(tile % lightsout.size !== 0) {
                lightsout.game[tile - 1] ^= 1;
            }
            if(tile % lightsout.size !== lightsout.size - 1) {
                lightsout.game[tile + 1] ^= 1;
            }
            if(tile - lightsout.size >= 0) {
                lightsout.game[tile - lightsout.size] ^= 1;
            }
            if(tile + lightsout.size < Math.pow(lightsout.size, 2)) {
                lightsout.game[tile + lightsout.size] ^= 1;
            }

            lightsout.render();
        },
        'render': function() {
            lightsout.board.empty();

            for(var tile in lightsout.game) {
                var newtile = $('<li class="tile ' + (lightsout.game[tile] === 1 ? 'tile-on' : '') + '" id="' + tile + '">');
                lightsout.board.append(newtile);
            }

            $('#clicks').text(lightsout.clicks + ' clicks');

            $('.tile').on('click', function() {
                lightsout.clicks+=1;
                lightsout.toggle(parseInt(this.id));
            });
        },
        'random': function() {
            lightsout.game = [];

            for(var i=0;i<Math.pow(lightsout.size, 2);i+=1) {
                lightsout.game.push(0);
            }

            for(var i=0;i<Math.pow(lightsout.size, 2);i+=1) {
                if(Math.random() <= 0.5) {
                    lightsout.toggle(i);
                }
            }

            lightsout.render();
        },
        'solve': function() {

            var solutions = {};

            solutions[[0,0,0,0,0]] = [];
            solutions[[0,0,1,1,1]] = [3];
            solutions[[0,1,0,1,0]] = [1,4];
            solutions[[0,1,1,0,1]] = [0];
            solutions[[1,0,0,0,1]] = [3,4];
            solutions[[1,0,1,1,0]] = [4];
            solutions[[1,1,0,1,1]] = [2];
            solutions[[1,1,1,0,0]] = [1];

            var size = lightsout.size,
                game = lightsout.game,
                move = 0;

            for( var pos=size; pos<size*size; pos++)
                if( game[pos-size] === 1) { lightsout.toggle(pos) }

            solutions[ game.slice(-size)].map( lightsout.toggle);

            for( var pos=size; pos<size*size; pos++)
                if( game[pos-size] === 1) { lightsout.toggle(pos) }
        }
    }

    lightsout.random();

    $('#reset').on('click', function() {
        lightsout.clicks = 0;
        lightsout.random();
    });


    $('#solve').on('click', function() {
        lightsout.solve();
    });
});
